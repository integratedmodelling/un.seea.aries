{
  "indicators.mountaingreencoverindex#Definition#Appendix" : {
    "trigger" : "DEFINITION",
    "sectionRole" : "APPENDIX",
    "section" : "Appendix",
    "documentedId" : "indicators.mountaingreencoverindex",
    "extended" : null,
    "template" : "@require(im.aries.seea.eea.appendix, Appendix)",
    "sectionType" : "BODY",
    "documentedUrns" : [ ],
    "eventType" : null,
    "variables" : { }
  },
  "indicators.mountaingreencoverindex#Definition#Methods" : {
    "trigger" : "DEFINITION",
    "sectionRole" : "METHODS",
    "section" : "Methods",
    "documentedId" : "indicators.mountaingreencoverindex",
    "extended" : null,
    "template" : "@section(The Mountain Green Cover Index - MGCI)\r\n\r\nWe follow three steps using standard methods for calculating MGCI @cite(UN2021). First, we classify land cover within mountain regions as either green (forest, shrubland, grassland, agricultural land, wetland) or non-green (artificial surfaces, water bodies, bare areas, sparse vegetation, and glacier/perpetual snow). Second, we split mountain zones into six elevation classes (300-1,000, 1,000-1,500, 1,500-2,500, 2,500-3,500, 3,500-4,500, and >4,500 meters above sea level). Third, we calculate MGCI for each mountain zone as: (mountain green cover area / total mountain area) * 100.\r\n\r\n-------------------------------------------------------------------------------------------\r\n\r\n| Mountain Class |                                                Description                                                |\r\n|:--------------:|:---------------------------------------------------------------------------------------------------------:|\r\n|        1       |                                          Elevation > 4500 meters                                          |\r\n|        2       |                                        Elevation 3500 - 4500 meters                                       |\r\n|        3       |                                        Elevation 2500 - 3500 meters                                       |\r\n|        4       |                                 Elevation 1500 - 2500 meters and slope > 2                                |\r\n|        5       | Elevation 1000 - 1500 meters and slope > 5 or local elevation range (LER 7 kilometer radius) > 300 meters |\r\n|        6       |        Elevation 300 - 1000 meters and local elevation range (LER 7 kilometer radius) > 300 meters        |\r\n\r\n-------------------------------------------------------------------------------------------\r\n\r\nHere the elevation range (LER) is not used in the classification criterias when detecting moutain classes (Mountain Class 5 & 6) in the model for computing the MGCI indicator, which differs from the SDG methodology.\r\n",
    "sectionType" : "BODY",
    "documentedUrns" : [ ],
    "eventType" : null,
    "variables" : { }
  },
  "indicators.mountaingreencoverindex#Definition#Introduction" : {
    "trigger" : "DEFINITION",
    "sectionRole" : "INTRODUCTION",
    "section" : "Introduction",
    "documentedId" : "indicators.mountaingreencoverindex",
    "extended" : null,
    "template" : "@require(im.aries.seea.eea.disclaimer, Introduction)\r\n\r\n@section(The Mountain Green Cover Index - MGCI)\r\n\r\n“The Mountain Green Cover Index (MGCI) is designed to measure the extent and the changes of green vegetation in mountain areas - i.e. forest, shrubs, trees, pasture land, crop land, etc. – in order to monitor progress towards the mountain target. MGCI is defined as the percentage of green cover over the total surface of the mountain region of a given country and for given reporting year. The aim of the index is to monitor the evolution of the green cover and thus assess the status of conservation of mountain ecosystems.” @cite(UN2021). For full metadata describing this indicator, see @cite(UN2021). This indicator is common to both the SDG and the CBD frameworks, but reported with different names:\r\n\r\n- SDG 15.4.2\r\n- CBD Complementary A.1.1.5\r\n",
    "sectionType" : "BODY",
    "documentedUrns" : [ ],
    "eventType" : null,
    "variables" : { }
  },
  "indicators.mountaingreencoverindex#Definition#Discussion" : {
    "trigger" : "DEFINITION",
    "sectionRole" : "DISCUSSION",
    "section" : "Discussion",
    "documentedId" : "indicators.mountaingreencoverindex",
    "extended" : null,
    "template" : "@section(The Mountain Green Cover Index - MGCI)\r\n\r\nRegarding the interpretation of the indicator, although in the great majority of cases the desired direction is an increase in green mountain cover which reflects restriction of damage to natural ecosystems and possibly even the expansion of forest, shrubland and grasslands through conservation efforts, in more limited cases, an increase in the indicator value in high elevation classes may also signify the encroachment of vegetation on areas previously covered by glaciers or other permanent or semi-permanent ice or snow layers, as a result of global warming due to climate change. Such a change can be tracked with the current methodology and flagged accordingly at the level of disaggregated data by land cover type and elevation class, to distinguish this case from the general desired trend of increasing mountain green cover @cite(UN2021) .\r\n",
    "sectionType" : "BODY",
    "documentedUrns" : [ ],
    "eventType" : null,
    "variables" : { }
  },
  "indicators.mountaingreencoverindex#Definition#Results" : {
    "trigger" : "DEFINITION",
    "sectionRole" : "RESULTS",
    "section" : "Results",
    "documentedId" : "indicators.mountaingreencoverindex",
    "extended" : null,
    "template" : "@section(The Mountain Green Cover Index - MGCI)\r\n\r\n---\r\n@figure(self, TheMountainGreenCover, Mountain green cover index)\r\n---\r\n\r\nThe results of the Mountain Green Cover Index (MGCI) is summarized in table.",
    "sectionType" : "BODY",
    "documentedUrns" : [ ],
    "eventType" : null,
    "variables" : { }
  }
}