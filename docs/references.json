{
  "UNDESA2021" : {
    "examplecitation" : "United Nations. 2021. System of Environmental-Economic Accounting - Ecosystem Accounting, Final Draft. Version 5 February 2021.",
    "key" : "UNDESA2021"
  },
  "YangEtAl2003" : {
    "examplecitation" : "D. Yang, S. Kanae, T. Oki, T. Koike, K. Musiake. Global potential soil erosion with reference to land use and climate changes. Hydrol. Process., 17 (14) (2003), pp. 2913-2928",
    "key" : "YangEtAl2003"
  },
  "KleinEtAl2007" : {
    "examplecitation" : "Klein, A.M., et al. 2007. Importance of pollinators in changing landscapes for world crops. Proceedings of the Royal Society of London B Biological Sciences 274 (1608):303–313.",
    "key" : "KleinEtAl2007"
  },
  "MartinezEtAl2019" : {
    "examplecitation" : "Martinez-Lopez et al. 2019. Towards globally customizable ecosystem service models. Science of the Total Environment 650(2):2325-2336.",
    "key" : "MartinezEtAl2019"
  },
  "MoodyEtAl1994" : {
    "examplecitation" : "Moody, A. and C.E. Woodcock. 1994. Scale-dependent errors in the estimation of land-cover proportions: Implications for global land-cover datasets. Photogrammetric Engineering and Remote Sensing 60:585-594.",
    "key" : "MoodyEtAl1994"
  },
  "ZulianEtAl2014" : {
    "examplecitation" : "Zulian, G., et al. 2014. ESTIMAP: a GIS-based model to map ecosystem services in the European Union. Annals of Botany 4:1–7.",
    "key" : "ZulianEtAl2014"
  },
  "Nordhaus2017" : {
    "examplecitation" : "Nordhaus, W.D. 2017. Revisiting the social cost of carbon. Proceedings of the National Academy of Sciences 114(7):1518-1523.",
    "key" : "Nordhaus2017"
  },
  "UNWTO2020" : {
    "examplecitation" : "U.N. World Tourism Organization (2020), Compendium of Tourism Statistics dataset [Electronic], UNWTO, updated on 20/01/2020.",
    "key" : "UNWTO2020"
  },
  "LonsdorfEtAl2009" : {
    "examplecitation" : "Lonsdorf, E., et al. 2009. Modelling pollination services across agricultural landscapes. Annals of Botany 103 (9):1589–1600.",
    "key" : "LonsdorfEtAl2009"
  },
  "UN2019" : {
    "examplecitation" : "United Nations. September 2019. Discussion paper 2.1: Purpose and role of ecosystem condition accounts. United Nations: New York.",
    "key" : "UN2019"
  },
  "VallecilloEtAl2019" : {
    "examplecitation" : "Vallecillo, S., et al. 2019. Ecosystem services accounting. Part II-Pilot accounts for crop and timber provision, global climate regulation and flood control, EUR 29731 EN, Publications Office of the European Union, Luxembourg. ISBN 978-92-76-02905-2, doi:10.2760/631588, JRC116334.",
    "key" : "VallecilloEtAl2019"
  },
  "UN2017" : {
    "examplecitation" : "United Nations. 2017. SEEA Experimental Ecosystem Accounting: Technical Recommendations. United Nations: New York.",
    "key" : "UN2017"
  },
  "VallecilloEtAl2018" : {
    "examplecitation" : "Vallecillo, S., et al. 2018. Ecosystem services accounting: Part I - Outdoor recreation and crop pollination, EUR 29024 EN; Publications Office of the European Union, Luxembourg, doi:10.2760/619793, JRC110321.",
    "key" : "VallecilloEtAl2018"
  },
  "BaroEtAl2016" : {
    "examplecitation" : "Baró, F., et al. 2016. Mapping ecosystem service capacity, flow and demand for landscape and urban planning: a case study in the Barcelona metropolitan region. Land Use Policy 57:405–417.",
    "key" : "BaroEtAl2016"
  },
  "im.aries.seea.eea.introduction" : {
    "examplecitation" : "**System of Environmental-Economic Accounting – Ecosystem Accounting (SEEA EA)**\r\n\r\nThe System of Environmental-Economic Accounting—Ecosystem Accounting (SEEA EA) is a spatially based, integrated statistical framework for organizing biophysical information about ecosystems, measuring ecosystem services, tracking changes in ecosystem extent and condition, valuing ecosystem services and assets and linking this information to measures of economic and human activity @cite(UNDESA2021).\r\n\r\nThe United Nations Statistics Division, in collaboration with the ARIES team, has developed a toolbox (the ARIES for SEEA Explorer) to better implement this accounting system globally. The ARIES for SEEA Explorer enables biophysical modelling, mapping, and valuation for various accounts including ecosystem extent, condition, physical and monetary supply and use, asset accounts, as well as some thematic accounts. Accounts can be compiled for any region on Earth using global data, with local data and parameters added to improve estimates for countries where local data are available.",
    "key" : "im.aries.seea.eea.introduction"
  },
  "ParacchiniEtAl2014" : {
    "examplecitation" : "Paracchini, M.L., et al. 2014. Mapping cultural ecosystem services: a framework to assess the potential for outdoor recreation across the EU. Ecological Indicators 45:371–385.",
    "key" : "ParacchiniEtAl2014"
  },
  "SayreEtAl2020" : {
    "examplecitation" : "Sayre, R., et al. 2020. An assessment of the representation of ecosystems in global protected areas using new maps of World Climate Regions and World Ecosystems. Global Ecology and Conservation 21:e00860.",
    "key" : "SayreEtAl2020"
  },
  "UNSEEA2017" : {
    "examplecitation" : "United Nations. 2017. Technical recommendations in support of the System of Environmental-Economic Accounting 2012. United Nations: New York City.",
    "key" : "UNSEEA2017"
  },
  "UNEtAl2014" : {
    "examplecitation" : "United Nations, European Commission, U.N. Food and Agriculture Organization, Organization for Economic Cooperation and Development, and World Bank. 2014. System of Environmental-Economic Accounting 2012, Experimental Ecosystem Accounting. United Nations: New York.",
    "key" : "UNEtAl2014"
  },
  "MousivandEtAl2019" : {
    "examplecitation" : "Mousivand, A. and J.J. Arsanjani. 2019. Insights on the historical and emerging global land cover changes: The case of ESA-CCI-LC datasets. Applied Geography 106: 82-92.",
    "key" : "MousivandEtAl2019"
  },
  "JoglekarEtAl2019" : {
    "examplecitation" : "Joglekar A.K.B., et al. 2019. Pixelating crop production: Consequences of methodological choices. PLoS ONE 14(2):e0212281.",
    "key" : "JoglekarEtAl2019"
  },
  "BalmfordEtAl2015" : {
    "examplecitation" : "Balmford, A. et al. 2015. Walk on the wild side: estimating the global magnitude of visits to protected areas. PLOS Biology 13(2):e1002074.",
    "key" : "BalmfordEtAl2015"
  },
  "WilliamsEtAl1995" : {
    "examplecitation" : "J.R. Williams, V.P. Singh. Computer models of watershed hydrology. The EPIC Model, Water Resources Publications, Highlands Ranch, CO (1995), pp. 909-1000.",
    "key" : "WilliamsEtAl1995"
  },
  "Tol2019" : {
    "examplecitation" : "Tol, R.S.J. 2019. A social cost of carbon for (almost) every country. Energy Economics 83:555-566.",
    "key" : "Tol2019"
  },
  "im.aries.seea.eea.disclaimer" : {
    "examplecitation" : "**Disclaimer**\r\n\r\nThe designations employed and the presentation of material on this map and any map used in this application do not imply the expression of any opinion whatsoever on the part of the Secretariat of the United Nations concerning the legal status of any country, territory, city or area or of its authorities, or concerning the delimitation of its frontiers or boundaries. This application allows users to select different backgrounds as maps. The Clear Map (https://www.un.org/geospatial/mapsgeo/webservices) is the official UN provided map. Other backgrounds such as Open Street Map are available for experimentation and to provide an enhanced user experience (in multiple languages). The results of the selected ecosystem accounts presented in this report are based on the context as specified by the user.",
    "key" : "im.aries.seea.eea.disclaimer"
  },
  "KeithEtAl2020" : {
    "examplecitation" : "Keith, D.A., et al. 2020. The IUCN Global Ecosystem Typology 2.0: Descriptive profiles for biomes and ecosystem functional groups. IUCN Commission on Ecosystem Management. IUCN: Gland, Switzerland. Available at: https://portals.iucn.org/library/node/49250, https://doi.org/10.2305/IUCN.CH.2020.13.en.",
    "key" : "KeithEtAl2020"
  },
  "UNEP1997" : {
    "examplecitation" : "UNEP. 1997. World atlas of desertification. United Nations Environment Programme.",
    "key" : "UNEP1997"
  },
  "im.aries.seea.eea.appendix" : {
    "examplecitation" : "**Definitions of countries and assignment of territories**\r\n\r\nThe ARIES for SEEA Explorer bridges statistical and geospatial domains which poses certain challenges. When classifying countries and areas, the M49 “Standard country or area codes for statistical use” (https://unstats.un.org/unsd/methodology/m49/) has been followed. Several politically contested territories listed below (Table 1) have been assigned to the noted country in order to attribute all land area on Earth (except Antarctica) to one country (ecosystem accounting area), as required by SEEA.\r\n\r\nTable 1. Adjustments made to the original UN M49 vector dataset to assign politically contested territories for accounting purposes.\r\n\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n\r\n| ISO-3alpha code |    Extended name   | M49 code |                                    Adjustments                                   |\r\n| :---------------: | :------------------| :--------: | :-------------------------------------------------------------------------------- |\r\n|       CHN       |        China       |    156   |                      Taiwan and Aksai Chin as part of China                      |\r\n|       EGY       |        Egypt       |    818   |                         Hala'ib Triangle as part of Egypt                        |\r\n|       IND       |        India       |    356   |               Arunachal Pradesh, Jammu and Kashmir as part of India              |\r\n|       KEN       |        Kenya       |    404   |                          Ilemi Triangle as part of Kenya                         |\r\n|       RUS       | Russian Federation |    643   |                          Kuril Islands as part of Russia                         |\r\n|       SHN       |    Saint Helena    |    654   | Ascencion, Gough, Saint Helena, Tristan da Cunha (UK) all merged as Saint Helena |\r\n|       PSE       | State of Palestine |    275   |                        Gaza, West Bank merged as Palestine                       |\r\n|       SDN       |        Sudan       |    729   |                            Bi'r Tawil as part of Sudan                           |\r\n\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n\r\nThe assignment of countries or areas to specific groupings is strictly for statistical purposes and does not imply official endorsement or acceptance by the United Nations. \r\n\r\nIn addition, there are a number of boundaries that are under dispute, including: \r\n\r\n- Final boundary between the Republic of Sudan and the Republic of South Sudan has not yet been determined.\r\n\r\n-The final status of Jammu and Kashmir has not yet been agreed upon by the parties.\r\n\r\n-A dispute exists between the Governments of Argentina and the United Kingdom of Great Britain and Northern Ireland concerning sovereignty over the Falkland Islands (Malvinas). \r\n\r\nThe user remains responsible for the setting of spatial and temporal context for which ecosystem accounts are compiled.",
    "key" : "im.aries.seea.eea.appendix"
  },
  "SharpEtAl2015" : {
    "examplecitation" : "R. Sharp, H.T. Tallis, T. Ricketts, A.D. Guerry, S.A. Wood, R. Chaplin-Kramer, et al. InVEST+ VERSION+ User's Guide. The Natural Capital Project, Stanford University, University of Minnesota, The Nature Conservancy, and World Wildlife Fund (2015).",
    "key" : "SharpEtAl2015"
  },
  "WangEtAl2019" : {
    "examplecitation" : "Wang, P., et al. 2019. Estimates of the social cost of carbon: A review based on meta-analysis. Journal of Cleaner Production 209:1494-1507.",
    "key" : "WangEtAl2019"
  },
  "DesmetEtAl1996" : {
    "examplecitation" : "Desmet, P.J.J. and Govers, G. (1996) A GIS Procedure for Automatically Calculating the USLE LS Factor on Topographically Complex Landscape Units. Journal of Soil and Water Conservation, 51, 427-433. - References - Scientific Research Publishing",
    "key" : "DesmetEtAl1996"
  },
  "SPAM2020" : {
    "examplecitation" : "Spatial Production Allocation Model (SPAM). 2020. MapSPAM. Accessed October 13, 2020 from: https://www.mapspam.info/.",
    "key" : "SPAM2020"
  },
  "PenaEtAl2015" : {
    "examplecitation" : "Peña, L., et al. 2015. Mapping recreation supply and demand using an ecological and social evaluation approach. Ecosystem Services 13:108–118.",
    "key" : "PenaEtAl2015"
  },
  "FAOSTAT2020" : {
    "examplecitation" : "FAOSTAT. 2020. FAOSTAT Food and Agriculture Data. Accessed October 13, 2020 from: http://www.fao.org/faostat/en/.",
    "key" : "FAOSTAT2020"
  },
  "RenardEtAl1997" : {
    "examplecitation" : "Renard, K.G., Foster, G.R., Weesies, G.A., McCool, D.K. and Yoder, D.C., 1997. Predicting soil erosion by water: a guide to conservation planning with the Revised Universal Soil Loss Equation (RUSLE) (Vol. 703). Washington, DC: United States Department of Agriculture.",
    "key" : "RenardEtAl1997"
  },
  "SantosEtAlInPrep" : {
    "examplecitation" : "Santos Martin, F. and Garcia Bruzon, A. In prep. Spanish forests experimental condition account. Universidad Rey Juan Carlos: Madrid.",
    "key" : "SantosEtAlInPrep"
  },
  "MonfredaEtAl2008" : {
    "examplecitation" : "Monfreda, C., et al. 2008. Farming the planet: 2. Geographic distribution of crop areas, yields, physiological types, and net primary production in the year 2000. Global Biogeochemical Cycles 22 (1):GB1022.",
    "key" : "MonfredaEtAl2008"
  },
  "BorrelliEtAl2017" : {
    "examplecitation" : "P. Borrelli, D.A. Robinson, L.R. Fleischer, E. Lugato, C. Ballabio, C. Alewell, et al. An assessment of the global impact of 21st century land use change on soil erosion. Nat. Commun., 8 (1) (2017), p. 2013.",
    "key" : "BorrelliEtAl2017"
  },
  "RueschEtAl2008" : {
    "examplecitation" : "Ruesch, Aaron, and Holly K. Gibbs. 2008. New IPCC Tier-1 Global Biomass Carbon Map for the Year 2000. Available online from the Carbon Dioxide Information Analysis Center [http://cdiac.ornl.gov], Oak Ridge National Laboratory, Oak Ridge, Tennessee.",
    "key" : "RueschEtAl2008"
  },
  "NaimiEtAl2020" : {
    "examplecitation" : "Naimi, B. 2020. Relative magnitude of fragmentation (RMF), forests. Accessed from https://portal.geobon.org/ebv-detail?id=4 on 22 March 2021.",
    "key" : "NaimiEtAl2020"
  }
}