private namespace seea.aries.es.crops.pollination;

/*
 * Crop production. Provisioning-only data will only give us those crops involved in the specific
 * service, so will this must stay local; need to merge the datasets before this can go in im.data.global.
 */
private model "local:ferdinando.villa:im.data.global:faostat.crop.pollination#collect=header->headerToCrop:>0"
	as agriculture:Crop within earth:Region;
	
private model "local:ferdinando.villa:im.data.global:crops.country.reference.totals#agriculture:Crop->headerToCrop"
	as im:Reference agriculture:Crop agriculture:Yield of geography:Country earth:Region in t;

private model "local:ferdinando.villa:im.data.global:faostat.crop.pollination#agriculture:Crop->headerToCropPrice"
	as monetary value of agriculture:Crop agriculture:Yield in USD@2015;
		
/*
 * Column is linked to type through categorization. Same story as before, must merge datasets, then this
 * can be brought into im.data.global.
 */
model "local:ferdinando.villa:im.data.global:faostat.crop.pollination#agriculture:Crop->headerToCrop"
	as agriculture:Crop agriculture:Yield of geography:Country earth:Region in t;		

@intensive(space)
private model agriculture:Yield caused by ecology:Pollination in t/ha
	observing 
		agriculture:Crop agriculture:Yield caused by ecology:Pollination in t/ha
	using klab.data.aggregate(semantics = agriculture:Crop agriculture:Yield caused by ecology:Pollination, nodata=true);

@intensive(space)
private model monetary value of agriculture:Yield caused by ecology:Pollination in USD@2015
	observing 
		monetary value of agriculture:Crop agriculture:Yield caused by ecology:Pollination in USD@2015/ha
	using klab.data.aggregate(semantics = monetary value of agriculture:Crop agriculture:Yield caused by ecology:Pollination, nodata=true);

@documented(seeaeea.pollination.insectoccurrence)
model occurrence of agriculture:Pollinator biology:Insect;

@intensive(space) 
model agriculture:Pollinated agriculture:Crop agriculture:Yield in t/ha
    observing
        im:Reference agriculture:Crop agriculture:Yield of geography:Country earth:Region in t
			named reference_country_yield,
		im:Reference agriculture:Crop agriculture:Yield in t/ha
			named reference_yield,
		agriculture:Crop agriculture:Yield of geography:Country earth:Region in t
			named country_yield,
        occurrence of agriculture:Pollinator biology:Insect 
        	named pollinator_occurrence,
        landcover:LandCoverType named landcover_type
    set to [(landcover_type is landcover:AgriculturalVegetation && pollinator_occurrence > 0.2) ? (country_yield * (reference_yield/reference_country_yield)) : unknown];

@documented(seeaeea.pollination.cropyield)
@intensive(space)
model agriculture:Crop agriculture:Yield caused by ecology:Pollination in t/ha
	observing 
		agriculture:Pollinated agriculture:Crop agriculture:Yield in t/ha  
			named pollinated_yield,
		proportion of agriculture:Crop agriculture:Yield caused by ecology:Pollination
			named pollination_dependent_proportion
	set to [ pollinated_yield * pollination_dependent_proportion];

@documented(seeaeea.pollination.cropyield)
@intensive(space)	
model monetary value of agriculture:Crop agriculture:Yield caused by ecology:Pollination in USD@2015/ha
	observing 
		monetary value of agriculture:Crop agriculture:Yield in USD@2015 named price_per_ton,
		agriculture:Crop agriculture:Yield caused by ecology:Pollination in t/ha named crop_yield
	set to [crop_yield * price_per_ton];  

//TODO: review aggregation on a static context 
@documented(seeaeea.pollination.tables.physicalaccount)	
define table crop_pollination_static as {
	title:  "Pollination contribution of crop production"
	label:  "Crop pollination"
	target:  agriculture:Yield caused by ecology:Pollination in t/ha
	when: end
	columns: (
			{ title:  "{classifier}", target: agriculture:Crop agriculture:Yield caused by ecology:Pollination }
	)
	rows:    { title: "Production (tons) in {start}" } 
};

@documented(seeaeea.pollination.tables.physicalaccount)	
define table crop_pollination_dynamic as {
	title:  "Pollination contribution of crop production"
	label:  "Crop pollination"
	target:  agriculture:Yield caused by ecology:Pollination in t/ha
	when: end
	columns: (
			{ title:  "{classifier}", target: agriculture:Crop agriculture:Yield caused by ecology:Pollination }
	)
	rows: (
		{ title: "Production {time} (tons)", filter: (start end) }
		{ title: "Net change", summarize: [r2 - r1], style: bold }
	)
};

//TODO: review aggregation on a static context
@documented(seeaeea.pollination.tables.monetaryaccount)	
define table pollinated_crop_value_static as {
	title:  "Monetary value of pollinated crops"
	label:  "Crop pollinated value"
	target:  monetary value of agriculture:Yield caused by ecology:Pollination in USD@2015
	when: end
	columns: (
			{ title: "{classifier}", target: monetary value of agriculture:Crop agriculture:Yield caused by ecology:Pollination }
//			{ title: "Total" }
	)
	rows:    { title: "Production (2015 USD) in {start}" } 
};

@documented(seeaeea.pollination.tables.monetaryaccount)	
define table pollinated_crop_value_dynamic as {
	title:  "Monetary value of pollinated crops"
	label:  "Crop pollinated value"
	target:  monetary value of agriculture:Yield caused by ecology:Pollination in USD@2015
	when: end
	columns: (
			{ title: "{classifier}", target: monetary value of agriculture:Crop agriculture:Yield caused by ecology:Pollination }
//			{ title: "Total" }
	)
	rows: (
		{ title: "Production {time} (2015 USD)", filter: (start end) }
		{ title: "Net change", summarize: [r2 - r1], style: bold }
	)
};

/**
 * Conversion factors for yield. TODO switch to a 'match (row = .., column = ....) to' 2-way table with countries on the rows and
 * crops on the columns, with a * for "other" countries. That could also match to a resource.
 */
model proportion of agriculture:Crop agriculture:Yield caused by ecology:Pollination
	lookup (agriculture:Crop) into
		agriculture:Apple       | 0.65,
		agriculture:Melon       | 0.9, 
		agriculture:Peach       | 0.65, 
		agriculture:Pear        | 0.65, 
		agriculture:Plum        | 0.65, 
		agriculture:Watermelon  | 0.9, 
		agriculture:Cucumber    | 0.65, 
		agriculture:Pumpkin     | 0.9, 
		agriculture:Cocoa       | 0.9, 
		agriculture:Mango       | 0.65, 
		agriculture:Avocado     | 0.65;
